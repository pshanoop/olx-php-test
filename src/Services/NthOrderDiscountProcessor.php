<?php


namespace App\Services;


use App\Collection\Collection;
use App\Entity\Invoice;
use App\Entity\Purchase;
use Money\Currency;
use Money\Money;

class NthOrderDiscountProcessor
{

    /** @var CurrencyFormatter */
    private $currencyFormatter;

    private $discountOrder;
    private $discountRate;

    /**
     * NthOrderDiscountProcessor constructor.
     * @param CurrencyFormatter $currencyFormatter
     * @param int $discountOrder
     * @param float $discountRate
     */
    const DISCOUNT_TYPE = 'NthMonthly';

    public function __construct(
        CurrencyFormatter $currencyFormatter,
        int $discountOrder = 3,
        float $discountRate = 10
    )
    {

        $this->discountOrder = $discountOrder;
        $this->discountRate = $discountRate;
        $this->currencyFormatter = $currencyFormatter;
    }

    /**
     * @param Collection $purchases
     * @return bool|Invoice
     */
    public function apply(Collection $purchases)
    {

        $purchase = $purchases->first();

        if ($purchase) {
            /** @var Purchase $purchase */
            $purchases->sortBy('getDate');

            $invoice = (new Invoice())
                ->setSupplierId($purchase->getSupplierId())
                ->setMoney($this->getAmountWithMonthlyDiscount($purchases))
                ->setDiscountType(self::DISCOUNT_TYPE)
            ;
            return $invoice;
        }

        return false;
    }

    private function getAmountWithMonthlyDiscount($purchases)
    {
        $numberOfOrders = [];

        $amount = $this->currencyFormatter->getMoney(0);

        /** @var Purchase $purchase */
        foreach ($purchases as $purchase) {

            $month = $purchase->getDate()->format('Ym');

            if (isset($numberOfOrders[$month])) {
                $numberOfOrders[$month]++;
                if (0 == $numberOfOrders[$month] % $this->discountOrder) {
                    $discount = $purchase->getMoney()
                        ->multiply(floatval($this->discountRate) / 100);

                    $discountedAmount = $purchase
                        ->getMoney()->subtract($discount)
                    ;

                    $purchase->setMoney($discountedAmount);
                }
            } else {
                $numberOfOrders[$month] = 1;
            }

            $amount = $amount->add($purchase->getMoney());
        }
        return $amount;
    }

}
