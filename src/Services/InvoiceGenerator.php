<?php


namespace App\Services;


use App\Collection\Collection;

class InvoiceGenerator
{
    /** @var DiscountProcessor */
    private $discountProcessor;

    public function __construct(DiscountProcessor $discountProcessor)
    {
        $this->discountProcessor = $discountProcessor;
    }

    /**
     * Generate Invoices with discounts applied.
     * @param Collection $suppliers
     * @return Collection
     */
    public function generate(Collection $suppliers): Collection
    {
        return $this->discountProcessor->applyDiscounts($suppliers);
    }

}
