<?php


namespace App\Services;


use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;

class CurrencyFormatter
{
    /** @var Currency */
    private $currency;
    /** @var ISOCurrencies  */
    private $currencies;
    /** @var DecimalMoneyFormatter */
    private $moneyFormatter;

    private $defaultCurrencyCode;

    /**
     * CurrencyFormatter constructor.
     * @param string $defaultCurrencyCode
     */
    public function __construct(string $defaultCurrencyCode = 'EUR')
    {
        $this->defaultCurrencyCode = $defaultCurrencyCode;
        $this->currencies = new ISOCurrencies();
        $this->moneyFormatter = new DecimalMoneyFormatter($this->currencies);
        $this->currency = new Currency($this->defaultCurrencyCode);
    }


    /**
     * Creates Money object from amount and code
     * @param string $amount
     * @param string $isoCode
     * @return Money
     */
    public function getMoney(string $amount='0', string  $isoCode = null): Money
    {
        $isoCode = $isoCode??$this->defaultCurrencyCode;
        return new Money($amount * 100, new Currency($isoCode));
    }


    /**
     * Returns string currency amount.
     * @param Money $money
     * @return string
     */
    public function getDecimal(Money $money): string
    {
        return $this->moneyFormatter->format($money);
    }


}
