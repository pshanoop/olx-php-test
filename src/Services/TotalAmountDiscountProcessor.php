<?php


namespace App\Services;


use App\Collection\Collection;
use App\Entity\Invoice;
use App\Entity\Purchase;
use Money\Money;

class TotalAmountDiscountProcessor
{

    /** @var CurrencyFormatter */
    private $currencyFormatter;
    /** @var Money */
    private $minimumTotal;
    /**
     * @var string
     */
    private $discountRate;

    /**
     * TotalAmountDiscountProcessor constructor.
     * @param CurrencyFormatter $currencyFormatter
     * @param string $minimumTotal
     * @param string $discountRate
     */
    const DISCOUNT_TYPE = 'TotalAmount';

    public function __construct(
        CurrencyFormatter $currencyFormatter,
        string $minimumTotal = '10000',
        string $discountRate = '5'
    )
    {
        $this->minimumTotal = $currencyFormatter
            ->getMoney($minimumTotal * 100);

        $this->currencyFormatter = $currencyFormatter;
        $this->discountRate = $discountRate;
    }

    /**
     * @param Collection $purchases
     * @return Invoice|bool
     */
    public function apply(Collection $purchases)
    {
        /** @var Purchase $purchase */
        $purchase = $purchases->first();

        if ($purchase) {

            $invoice = (new Invoice())
                ->setSupplierId($purchase->getSupplierId())
                ->setMoney($this->getTotalAmount($purchases))
                ->setDiscountType(self::DISCOUNT_TYPE)
            ;
            return $invoice;
        }

        return false;
    }

    /**
     * Make total amount including discount applied.
     * @param Collection $purchases
     * @return Money
     */
    private function getTotalAmount(Collection $purchases): Money
    {

        $sum = $this->currencyFormatter->getMoney(0);

        foreach ($purchases as $purchase) {
            /** @var Purchase $purchase */
            $sum = $sum->add($purchase->getMoney());
        }

        if ($sum->greaterThanOrEqual($this->minimumTotal)) {
            $discount = $sum->multiply(
                floatval($this->discountRate) / 100
            );
            $sum = $sum->subtract($discount);
        }

        return $sum;

    }
}
