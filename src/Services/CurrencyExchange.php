<?php


namespace App\Services;


use Money\Converter;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Exchange\FixedExchange;
use Money\Exchange\ReversedCurrenciesExchange;
use Money\Money;

class CurrencyExchange
{

//     TODO: Move this to service config
    protected $fixedExchanges = [
        'EUR' => [
            'USD' => 1.21,
            'GBP' => 0.87,
            'EUR' => 1 //Temp fix for same currency conversion.
        ]
    ];

    protected $converter;
    private $defaultCurrency;

    public function __construct(string $defaultCurrency = 'EUR')
    {
        $this->defaultCurrency = new Currency($defaultCurrency);

        $exchange = new FixedExchange($this->fixedExchanges);
        $exchange = new ReversedCurrenciesExchange($exchange);

        $this->converter = new Converter(new ISOCurrencies(), $exchange);
    }

    /**
     * Exchange given money to default money.
     * @param Money $money
     * @return Money
     * TODO: Validate $money on if currency exists in fixed exchange config.
     */
    public function convert(Money $money): Money
    {
        return $this->converter
            ->convert($money, $this->defaultCurrency);
    }

}
