<?php


namespace App\Services;


use App\Collection\Collection;
use App\Entity\Purchase;
use Money\Currency;
use Money\Money;

class PurchaseFileProcessor
{

    /** @var  bool|resource */
    protected $fileDescriptor;

    /** @var Collection */
    protected $records;
    /** @var CurrencyExchange */
    protected $currencyExchange;
    /** @var CurrencyFormatter */
    private $currencyFormatter;

    protected $homeCurrency = 'EUR';


    public function __construct(
        CurrencyExchange $currencyExchange,
        CurrencyFormatter $currencyFormatter
    )
    {

        $this->records = new Collection(Collection::class);
        $this->currencyExchange = $currencyExchange;
        $this->currencyFormatter = $currencyFormatter;
    }

    public function read(string $file): Collection
    {
        $this->fileDescriptor = fopen($file, 'r');
        return $this->process();
    }


    protected function process(): Collection
    {
        while (($csvLine = fgets($this->fileDescriptor)) !== false) {
            $purchase = $this->getPurchase(trim($csvLine));

            if (isset($this->records[$purchase->getSupplierId()])) {
                $this->records[$purchase->getSupplierId()][] = $purchase;
            } else {
                $this->records[$purchase->getSupplierId()] = new Collection(Purchase::class);
                $this->records[$purchase->getSupplierId()][] = $purchase;
            }
        }
        fclose($this->fileDescriptor);
        return $this->records;
    }

    /**
     * @param $csvLine
     * @return Purchase
     */
    protected function getPurchase($csvLine)
    {
        $purchase = new Purchase($csvLine);

        $purchase = $this->setMoney($purchase);
//        $money = $purchase->getMoney();
        $money = $this->currencyExchange->convert($purchase->getMoney());

        //Reset amount and currency code.
        $purchase
            ->setAmount($this->currencyFormatter->getDecimal($money))
            ->setCurrencyCode($money->getCurrency()->getCode());
        $purchase->setMoney($money);

        return $purchase;
    }

    protected function setMoney(Purchase $purchase)
    {
        $money = $this->currencyFormatter
            ->getMoney($purchase->getAmount(), $purchase->getCurrencyCode());
        return $purchase
            ->setMoney($money);
    }

}