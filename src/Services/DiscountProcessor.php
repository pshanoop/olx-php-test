<?php


namespace App\Services;


use App\Collection\Collection;
use App\Entity\Invoice;

class DiscountProcessor
{
    /** @var NthOrderDiscountProcessor */
    private $nthOrderDiscountProcessor;
    /** @var TotalAmountDiscountProcessor */
    private $totalAmountDiscountProcessor;

    public function __construct(
        NthOrderDiscountProcessor $nthOrderDiscountProcessor,
        TotalAmountDiscountProcessor $totalAmountDiscountProcessor
    )
    {

        $this->nthOrderDiscountProcessor = $nthOrderDiscountProcessor;
        $this->totalAmountDiscountProcessor = $totalAmountDiscountProcessor;
    }


    /**
     * @param Collection $suppliers
     * @return Collection
     */
    public function applyDiscounts(Collection $suppliers)
    {
        $invoices = new Collection(Invoice::class);
        foreach ($suppliers as $supplier => $purchases) {

            $nOrderDiscountInvoice = $this->nthOrderDiscountProcessor->apply($purchases);

            $invoices [] = $this->minimalInvoice(
                $nOrderDiscountInvoice,
                $this->totalAmountDiscountProcessor->apply($purchases)
            );
        }

        return $invoices;
    }


    /**
     * Select lowest Invoice amount
     * @param Invoice $invoiceA
     * @param Invoice $invoiceB
     * @return Invoice
     */
    protected function minimalInvoice(Invoice $invoiceA, Invoice $invoiceB)
    {
        if ($invoiceA->getMoney()->lessThan($invoiceB->getMoney())) {
            return $invoiceA;
        }

        return $invoiceB;
    }

}
