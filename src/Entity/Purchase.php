<?php


namespace App\Entity;


use Carbon\Carbon;
use Money\Currency;
use Money\Money;

class Purchase
{
    private $supplierId;

    /** @var Carbon */
    private $date;
    private $currencyCode;

    /** @var  Money */
    private $money;

    /**
     * Purchase constructor.
     * @param string $csvString csv string in supplierId,date,amount,currency format.
     * TODO add exception for bad format
     */
    public function __construct(string $csvString = null)
    {
        if (isset($csvString)) {

            $data = explode(',', $csvString);
            $this
                ->setSupplierId($data[0])
                ->setDate($data[1])
            ;
            $this->money = new Money($data[2] * 100, new Currency($data[3]));
        }
    }

    /**
     * @param mixed $supplierId
     * @return Purchase
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param mixed $date
     * @return Purchase
     */
    public function setDate($date)
    {
        $this->date = Carbon::createFromFormat('Y-m-d',$date);
        return $this;
    }

    /**
     * @param mixed $amount
     * @param $currencyCode
     * @return Purchase
     */
    public function setAmount(string $amount, string $currencyCode = 'EUR')
    {
        $this->money = new Money($amount*100, new Currency($currencyCode));
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->money->getCurrency()->getCode();
    }

    /**
     * @return Carbon
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $currencyCode
     * @return Purchase
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * Amount without fractions.
     * @return mixed
     */
    public function getAmount()
    {
        return $this->money->getAmount();
    }

    /**
     * @param Money $money
     * @return Purchase
     */
    public function setMoney(Money $money): Purchase
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @return Money
     */
    public function getMoney(): Money
    {
        return $this->money;
    }
}
