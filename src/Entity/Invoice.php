<?php


namespace App\Entity;


use Money\Currency;
use Money\Money;

class Invoice
{
    private $supplierId;
    private $discountType;


    /** @var Money */
    private $money;


    /**
     * @param mixed $supplierId
     * @return Invoice
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param mixed $amount
     * @param string $currencyCode
     * @return Invoice
     */
    public function setAmount(string $amount, string $currencyCode = 'EUR')
    {
        $this->money = new Money($amount * 100, new Currency($currencyCode));
        return $this;
    }

    /**
     * Amount without fractions
     * @return mixed
     */
    public function getAmount()
    {
        return $this->money->getAmount();
    }


    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->money->getCurrency()->getCode();
    }

    /**
     * @param Money $money
     * @return Invoice
     */
    public function setMoney(Money $money): Invoice
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @return Money
     */
    public function getMoney(): Money
    {
        return $this->money;
    }

    /**
     * @param mixed $discountType
     * @return Invoice
     */
    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;
        return $this;
    }

    /**
     * @return string
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

}
