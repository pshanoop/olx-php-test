<?php

namespace App\Command;

use App\Renderers\InvoiceListRender;
use App\Renderers\SupplierPurchaseListRender;
use App\Services\InvoiceGenerator;
use App\Services\PurchaseFileProcessor;
Use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class InvoiceGenerateCommand extends Command
{
    protected $inputFile;

    // Argument name for file
    const ARG_FILE = 'file';

    /** @var PurchaseFileProcessor */
    private $fileProcessor;

    /** @var SupplierPurchaseListRender */
    private $supplierPurchaseListRender;
    /** @var InvoiceGenerator */
    private $invoiceGenerator;
    /** @var InvoiceListRender */
    private $invoiceListRender;

    public function __construct(
        PurchaseFileProcessor $fileProcessor,
        SupplierPurchaseListRender $supplierPurchaseListRender,
        InvoiceGenerator $invoiceGenerator,
        InvoiceListRender $invoiceListRender
    )
    {
        parent::__construct();
        $this->fileProcessor = $fileProcessor;
        $this->supplierPurchaseListRender = $supplierPurchaseListRender;
        $this->invoiceGenerator = $invoiceGenerator;
        $this->invoiceListRender = $invoiceListRender;
    }

    protected function configure()
    {
        $this
            ->setName('app:invoice:generate')
            ->setDescription('Generate invoice CSV output from purchases CSV file.')
            ->addArgument(self::ARG_FILE, InputArgument::REQUIRED, 'CSV file of purchases.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->processArguments($input);
        $output->writeln('Processing file: ' . $this->inputFile);
        $suppliers = $this->processData();

        $invoices = $this->invoiceGenerator->generate($suppliers);

//        $this->supplierPurchaseListRender
//            ->render($output, $suppliers);

        $output->writeln(PHP_EOL . 'Invoices: ');

        $this->invoiceListRender->render($output, $invoices);
    }

    private function processArguments(InputInterface $input)
    {
        $this->inputFile = $input->getArgument(self::ARG_FILE);
    }

    private function processData()
    {
        return $this->fileProcessor->read($this->inputFile);
    }
}
