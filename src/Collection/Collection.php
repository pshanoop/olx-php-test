<?php

namespace App\Collection;

/**
 * Class Collection
 * @package App\Collection
 *
 * Tiny object collection implementation.
 */

class Collection implements \ArrayAccess, \IteratorAggregate, \Countable
{
    private $objectType;

    private $items = [];

    public function __construct(string $type)
    {
        $this->objectType = $type;
    }

    public function offsetExists($offset)
    {
        return isset($this->items[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->items[$offset];
    }

    public function offsetSet($offset, $item)
    {

        if (!is_a($item, $this->objectType)) {

            throw new \UnexpectedValueException();
        }
        if (is_null($offset)) {
            $this->items[] = $item;
        } else {
            $this->items[$offset] = $item;
        }
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function count()
    {
        return count($this->items);
    }

    /**
     * Sorts by Method or Attribute of collection type
     * @param string $attribute or method
     */
    public function sortBy(string $attribute): void
    {
        usort($this->items, function ($a, $b) use ($attribute) {
            if (property_exists($a, $attribute) && property_exists($b, $attribute)) {
                return $a->{$attribute} <=> $b->{$attribute};
            } else if (method_exists($a, $attribute) && method_exists($b, $attribute)) {
                return $a->{$attribute}() <=> $b->{$attribute}();
            }
            return false;
        });
    }

    /**
     * Gets the first object in collection
     * @return mixed
     */
    public function first()
    {
        return $this->items[0] ?? null;
    }
}
