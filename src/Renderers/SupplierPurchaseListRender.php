<?php


namespace App\Renderers;


use App\Collection\Collection;
use App\Entity\Purchase;
use App\Services\CurrencyFormatter;
use Symfony\Component\Console\Output\OutputInterface;

class SupplierPurchaseListRender
{

    /** @var CurrencyFormatter */
    private $currencyFormatter;

    public function __construct(CurrencyFormatter $currencyFormatter)
    {

        $this->currencyFormatter = $currencyFormatter;
    }
    public function render(OutputInterface $output, Collection $suppliers)
    {
        foreach ($suppliers as $supplierId => $purchases) {
            $output->write([PHP_EOL, 'SupplierId: ', $supplierId, PHP_EOL]);
            /** @var Collection $purchases */
            $purchases->sortBy('getDate');
            /** @var Purchase $purchase */
            foreach ($purchases as $purchase) {
                $output->writeln(implode(' ', [
                    $purchase->getSupplierId(),
                    $purchase->getDate()->format('Y-m-d'),
                    $this->currencyFormatter->getDecimal($purchase->getMoney()),
                    $purchase->getCurrencyCode()
                ]));
            }
        }
    }

}
