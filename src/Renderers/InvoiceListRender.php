<?php


namespace App\Renderers;


use App\Collection\Collection;
use App\Entity\Invoice;
use App\Services\CurrencyFormatter;
use Symfony\Component\Console\Output\OutputInterface;

class InvoiceListRender
{
    /** @var CurrencyFormatter */
    private $currencyFormatter;

    public function __construct(CurrencyFormatter $currencyFormatter)
    {

        $this->currencyFormatter = $currencyFormatter;
    }


    public function render(OutputInterface $output, Collection $invoices)
    {

        /** @var Invoice $invoice */
        foreach ($invoices as $invoice) {
            $output->writeln( implode(',', [
                $invoice->getSupplierId(),
                $this->currencyFormatter->getDecimal($invoice->getMoney()),
                $invoice->getCurrencyCode(),
                $invoice->getDiscountType()
            ]));
        }

    }

}
