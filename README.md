### Test

This a sample CLI application to process csv purchase list  to Invoices with certain discount methods.  It's based on [symfony 4](https://symfony.com/doc/current/setup.html)

To run
`composer install`

`bin/console app:invoice:generate ../extras/example-input.csv` 
 Here last argument is path to the csv purchase list file.



### TODO

- Add unit test files.
- Remove stub files from `symfony/skeleton`
- Add config for services with default parameters values